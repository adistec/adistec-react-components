import React from 'react';
import styled from 'styled-components';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'react-select-adistec';
import { connect } from 'react-redux';
import { Field,change,untouch } from 'redux-form';
import 'react-select-adistec/dist/react-select';
import { FormattedMessage } from 'react-intl';
import ClearButtonSelectField from './ClearButtonSelectField.jsx';
import './SelectField.scss';
import isEmpty from 'lodash/isEmpty';


const StyledItem = styled.span`
    white-space:nowrap;
    position:relative;
`;

const StyledSelectedItem = styled(StyledItem)`
    padding-left:5px;

    i{
        display:block;
        position:absolute;
        top:-8px;
        bottom:-8px;
        left:-10px;
        border-left:solid 3px #1226aa !important;
    }

`;

const renderMultipleOption = (option,i,inputValue,valueArray) => {
    if(valueArray.filter(valueElement => valueElement.value == option.value).length > 0){
        return <StyledSelectedItem><i />{option.label}</StyledSelectedItem>;
    }
    else {
        return <StyledItem>{option.label}</StyledItem>;
    }
};

const RenderSelectInput = (props) => (
    <div className={`
        ${!isEmpty(props.input.value) ? 'item-selected' : ''}
        ${props.userType ? props.userType : 'adistec'}
    `}>
        <label className="select--multi-floatinglabel">{props.label}</label>
        <SelectField
            {...props}
            className={`
                ${props.userType ? props.userType : 'adistec'}
                ${props.meta.touched && props.meta.error ? 'error' : null}
            `}
            customSelectedItemTemplate={props.multi ? 
                (values) => {
                    if(values.length > 1)
                       {return <span>{values.length + ' '}{<FormattedMessage id="selectField.multiSelectInputMessage"/>}</span>;}
                    else
                        {return <span>{values[0].label}</span>;}
                } : null }
            renderOption={props.multi && !props.renderOption ? props.renderMultipleOption : props.renderOption}
            value={props.input.value}
            onChange={(value) => {
                return props.multi ? props.input.onChange(value.map(p => p.value)) :
                    props.input.onChange(value ? value.value : '');
            }}
            onBlur={() => {
                if(props.multi) return props.input.onBlur([...props.input.value]);}
            }
            onValueClick={() => null}
            closeOnSelect={props.multi ? false : true}
            clearable={false}
        />
        {props.meta.touched && props.meta.error ? <span className="error">{props.meta.error}</span> : null}
    </div>

);

class AdistecSelect extends React.Component {
    constructor(props) {
        super(props);
    }

    reset = () => {
        this.props.resetSelectField(this.refs[this.props.name].context._reduxForm.form,this.props.name,null);
        this.props.untouchSelectField(this.refs[this.props.name].context._reduxForm.form,this.props.name);
    }

    render(){
        return(
            <div className={`${this.props.size ? 'adistec-select-field col s' + this.props.size : ''} ${this.props.multiple ? 'multiple' : ''}`} style={{ position: 'relative' }}>
                <Field 
                        userType={this.props.userType}
                        floatingLabelText={this.props.label}
                        onBlurResetsInput={this.props.onBlurResetsInput}
                        onSelectResetsInput={this.props.onSelectResetsInput}
                        component={RenderSelectInput}
                        className={this.props.className}
                        onChange={this.handleChange}
                        placeholder={null}
                        closeOnSelect={this.props.closeOnSelect}
                        multi={this.props.multi}
                        backspaceRemoves={false}
                        removeSelected={false}
                        maxSearchResults = {this.props.maxSearchResults}
                        noResultsText={this.props.noResultsText}
                        customSelectedItemTemplate={this.props.customSelectedItemTemplate}
                        optionRenderer={this.props.renderOption}
                        validate={this.props.validate}
                        withRef={this.props.clearButton ? true : false}
                        ref={this.props.clearButton ? this.props.name : null}
                        name={this.props.name}
                        options={this.props.items ? this.props.items : null}
                        pageSize={this.props.maxSearchResults? this.props.maxSearchResults : 5 }
                        searchable={this.props.autocomplete}
                        label={this.props.label}
                />
                 {this.props.clearButton ? <ClearButtonSelectField userType={this.props.userType} onClick={this.reset}/> : null}
            </div>
        );
    }
}

AdistecSelect.defaultProps = {
    renderOption:renderMultipleOption,
    onBlurResetsInput:true,
    onSelectResetsInput:true,
    noResultsText: <FormattedMessage id="autocomplete.noResultsFound"/>,
    maxSearchResults: 5,
    autocomplete:false
};

export default connect(null, {
  resetSelectField: (formName,fieldName,value) => change( formName, fieldName, value ),
  untouchSelectField: (formName,fieldName) => untouch(formName,fieldName)
})(AdistecSelect);
