import React from 'react';
import styled from 'styled-components';
import TextField  from 'redux-form-material-ui/es/TextField';
import { connect } from 'react-redux';
import { Field, change, untouch } from 'redux-form';
import ClearButtonTextField from './ClearButtonTextField.jsx';


const propTypes = {
    className: React.PropTypes.string,
    clearButton: React.PropTypes.bool,
    copyButton: React.PropTypes.boolean,
    disabled: React.PropTypes.bool,
    label: React.PropTypes.string,
    maxLength: React.PropTypes.string,
    multiLine: React.PropTypes.bool,
    name: React.PropTypes.string,
    normalize: React.PropTypes.function,
    onChange: React.PropTypes.function,
    onClick: React.PropTypes.function,
    rows: React.PropTypes.array,
    size: React.PropTypes.string,
    type: React.PropTypes.string,
    validate: React.PropTypes.array
};


const StyledTextField = styled(TextField)`
    width: 100% !important;
    box-sizing: border-box !important;
    margin-bottom: 13px !important;
    font-size: 14px !important;
    padding-right: 13px;
    
    /* Text floating label */
    label {
        top: 45% !important;
        white-space:nowrap;
        width:100%;
        overflow:hidden;
        text-overflow:ellipsis;
    }

    &.adistec-textarea{
        margin-top: 22px !important;
        label {
            top:auto !important;
        }
    }
        
    /* Text input */
    input {
        margin-top: 22px !important;
        height: 50% !important;
    }
	
    /* Text error field */
    div:nth-child(4) {
        top: 5px !important;
        float: left !important;
    }
`;


const CustomTextField = (props) => {
    const { userType, className, input, clearButton, placeholder, InputProps, copyButton,reset,size,floatingLabelText,floatingLabelFixed,type,meta:{ touched, error, warning },disabled, onClick, onChange, maxLength } = props;
    return(
    <div className={size ? 'col s' + size : ''} style={{ position:'relative' }}>
        <StyledTextField type={type} placeholder={placeholder} InputProps={InputProps} className={className}
                         maxLength={maxLength} onChange={onChange} onClick={onClick}
                         floatingLabelText={floatingLabelText} style={props.multiLine ? null : { height:'62px' }} {...input}
                         multiLine={props.multiLine ? props.multiLine : false} disabled={disabled}
                         rows={props.rows ? props.rows : null}
                         rowsMax={props.rowsMax ? props.rowsMax : null}
                         errorText={touched ? error : null}
                         userType={userType}
                         floatingLabelFocusStyle={userType === 'bpod' ? { color: '#13017c' } : { color: '#1E2CA2' }}
                         underlineFocusStyle={userType === 'bpod' ? { borderColor: '#13017c' } : { borderColor: '#1E2CA2' }}
                         floatingLabelFixed={floatingLabelFixed}
        />
        {clearButton && touched ? <ClearButtonTextField onClick={reset}/> : null}
    </div>);
};

class AdistecTextField extends React.Component {
    constructor(props) {
        super(props);
     }

    reset = (e) => {
        this.props.resetTextField(this.refs[this.props.name].context._reduxForm.form,this.props.name,null);
        this.props.untouchTextField(this.refs[this.props.name].context._reduxForm.form,this.props.name);
        this.props.onChange ? this.props.onChange(e, null) : null;
    };


    render() {
        return(
            <Field name={this.props.name}
                {...this.props}
                component={CustomTextField}
                size={this.props.size}
                clearButton={this.props.clearButton}
                floatingLabelText={this.props.label}
                className={this.props.className}
                withRef
                ref={this.props.name}
                onClick={this.props.onClick}
                maxLength={this.props.maxLength}
                onChange={this.props.onChange}
                reset={this.reset}
                type={this.props.type}
                validate={this.props.validate}
                normalize={this.props.normalize}
                />
    );}
}

AdistecTextField.propTypes = propTypes;

export default connect(null, {
  resetTextField: (formName,fieldName,value) => change( formName, fieldName, value ),
  untouchTextField: (formName,fieldName) => untouch(formName,fieldName)
},null,{ withRef:true })(AdistecTextField);