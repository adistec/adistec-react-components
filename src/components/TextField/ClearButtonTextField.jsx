import React from 'react';
import styled from 'styled-components';

const StyledSpan = styled.span`
    margin-top: 29px;
    height: 50%;
    position: absolute;
    right:10px;
    top:0px; 
    color:#CCC; 
    cursor:pointer;
    font-size:12px;
    transition: all 150ms cubic-bezier(0.23, 1, 0.32, 1) 0ms !important;
    
    &:hover{
    	color: ${props => props.userType === 'bpod' ? '#13017c':'var(--adistecBlue)'};
    }
`;

const ClearButtonTextField = (props) => {
	return (<StyledSpan {...props}>
    	<i className="fa fa-times" aria-hidden="true" />
    </StyledSpan>);
};

export default ClearButtonTextField;