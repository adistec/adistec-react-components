import React from 'react';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';

const Button = styled(RaisedButton)`
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12) !important;
    width: 100%;
    margin-bottom: inherit;
    margin-top: inherit;
    vertical-align: middle !important;
    z-index: 1 !important;
    will-change: opacity, transform !important;
    transition: all .3s ease-out !important;
    
    button {
        background-color: ${props => props.userType === 'bpod' ? '#13017c !important' : 'var(--adistecBlue) !important'};
        
        &:hover {
            background-color: ${props => props.userType === 'bpod' ? '#1226aa !important' : 'var(--adistecLightBlue) !important'};
        }
        
        div {
            div { 
                span {
                    color: #FAFAFA !important;
                }
            }
        }
    }
    
        
    .cancel>button{
      background-color:#999 !important;    
      &:hover{
        background-color:#CCC !important;
      }
    }
    
    @media screen and (max-width: 1268px) {
        span {
            font-size: 12px !important;
            padding: 0 !important;
        }
        
        i {
            margin-right: 8px !important;
            margin-left: 0 !important;
        }
    }
`;

const propTypes = {
    className:React.PropTypes.string,
    disabled: React.PropTypes.boolean,
    icon:React.PropTypes.string,
    label:React.PropTypes.string,
    marginBottomTop: React.PropTypes.string,
    marginRight:React.PropTypes.string,
    onClick:React.PropTypes.func,
    type: React.PropTypes.string,
    width: React.PropTypes.string
};

const AdistecButton = ({
    label,
    onClick,
    // to,
    width,
    marginBottomTop,
    marginRight,
    disabled,
    className,
    icon,
    type,
    userType
}) => (
    <div style={
        {
            marginTop: `${marginBottomTop ? marginBottomTop : '0'}px`,
            marginBottom: `${marginBottomTop ? marginBottomTop : '0'}px`,
            marginRight: `${marginRight ? marginRight : '0'}px`,
            width: `${width ? width : '100'}%`,
            float: `${className ? className : 'initial'}`
        }
    }>

        <Button userType={userType} onTouchTap={onClick} label={label} disabled={disabled} className={className} icon={icon} type={type}/>

    </div>
);

AdistecButton.propTypes = propTypes;

export default AdistecButton;