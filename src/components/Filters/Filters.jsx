import React from 'react';
import { reduxForm , getFormValues , getFormSyncErrors , touch } from 'redux-form';
import { connect } from 'react-redux';
import difference from 'lodash/difference';
import pick from 'lodash/pick';
import omitBy from 'lodash/omitBy';
import isNil from 'lodash/isNil';
import intersection from 'lodash/intersection';
import get from 'lodash/get';
import moment from 'moment';
import fuse from 'fuse.js';
import { FormattedMessage,injectIntl } from 'react-intl';
import './Filters.scss';

/*config structure
    {
        filter-name:{
            type:string, <-- opcional can be date,boolean,freeText
            customKey:string, <-- opcional
            part:string, <-- only on date can be day,mounth, year etc(check moment) defauly day
            operator:string, <-- only on date can be >,<,>=,<=,== default ==
            fuseConfiguration:object <-- only on freeText check fuse.js default in getDefaultFuseOptions function
        }
    }
*/
const propTypes = {
    afterApplyFilter: React.PropTypes.func.isRequired,
    applyLabel:React.PropTypes.object,
    beforeApplyFilter: React.PropTypes.func, // has to be a promise
    config:React.PropTypes.object,
    containerClass: React.PropTypes.string,
    data:React.PropTypes.array,
    excludedKeys: React.PropTypes.array,
    fields:React.PropTypes.array.isRequired,
    forceSubmit:React.PropTypes.func,
    handleSubmit:React.PropTypes.func,
    locale:React.PropTypes.string,
    open:React.PropTypes.func,
    title:React.PropTypes.object,
    titleButtons: React.PropTypes.node,
    viewHideShowButtons: React.PropTypes.bool,
    viewLessLabel:React.PropTypes.object,
    viewMoreButton: React.PropTypes.bool,
    viewMoreLabel:React.PropTypes.object
};

const defaultProps = {
    viewMoreButton: false,
    excludedKeys: [],
    beforeApplyFilter: () => Promise.resolve(),
    applyLabel: <FormattedMessage id="filters.apply"/>,
    viewMoreLabel: <FormattedMessage id="filters.viewMore"/>,
    viewLessLabel: <FormattedMessage id="filters.viewLess"/>,
    title: <FormattedMessage id="filters.title"/>
};

const isValidDateInput = (date) => {
    const ISO8601Regex = new RegExp('^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])(T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?)?$');

    if (date instanceof Date){
        return true;
    }else{
        return !date || ISO8601Regex.test(date);
    }
};

const evaluateDateCondition = (firstArgument,operator,secondArgument,part) => {
    if (!isValidDateInput(firstArgument)){
        const err = new Error('row must be either Date or valid ISO8601 string');
        throw err;
    }

    if (!isValidDateInput(secondArgument)){
        const err = new Error('filter must be either Date or valid ISO8601 string');
        throw err;
    }

    switch(operator){
        case '>':
            return  moment(firstArgument).isAfter(moment(secondArgument),part);
        case '<':
            return  moment(firstArgument).isBefore(moment(secondArgument),part);
        case '>=':
            return  moment(firstArgument).isSameOrAfter(moment(secondArgument),part);
        case '<=':
            return  moment(firstArgument).isSameOrBefore(moment(secondArgument),part);
        default:
            return  moment(firstArgument).isSame(moment(secondArgument),part);
    }
};


class AdistecFilters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: this.props.open || false,
            hidden: this.props.hidden || false,
            locale: 'es'
        };

    }

    componentWillMount() {
        if (this.props.pristine) {
            let initalValues = this.loadInitialValues();
            this.props.initialize(initalValues);
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.forceSubmit && this.props.forceSubmit != nextProps.forceSubmit){
            this.handleApply();
        }
    }

    handleShowMore = () => {
        let open = true;
        this.setState({ open: !open });
    }

    isEmptyArray = (value) => {
        return Array.isArray(value) && value.length == 0 ? true : false;
    }

    isEmptyFilter = (value) => {
        return isNil(value) || value === '';
    }

    handleApply = () => { //Al hacer click en aplicar
        this.props.beforeApplyFilter().then(() => {
            let includeEntires = difference(Object.keys(this.props.formValues),this.props.excludedKeys);
            let filters = pick(omitBy(omitBy(this.props.formValues, this.isEmptyFilter),this.isEmptyArray),includeEntires);
            let excludedFilters = pick(omitBy(omitBy(this.props.formValues, isNil),this.isEmptyArray),this.props.excludedKeys);
            let config =  this.props.config;
            this.props.afterApplyFilter(this.buildFilter(this.props.data,filters,config),filters);
        });
    }

    handleExpend = () => {
        this.setState({ open:!this.state.open });
    }

    handleShowHide = () => {
        this.setState({ hidden:!this.state.hidden });
    }

    loadInitialValues = () => {
        let initalValues = {};
        this.props.fields.map((field) =>
            field.props.value ? initalValues[field.props.name] = field.props.value : null
        );
        return initalValues;
    }

    getDefaultFuseOptions = (list,key) => {
        var options = {
            shouldSort: true,
            threshold: 0.1,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: [key]
        };
        return new fuse(list, options);
    }

    buildFilter = (array, filters,config) =>{
        for (var key in filters){
            let isMultiSelect = (this.props.fields.filter(f => {return  f.props.name == key;})[0]? this.props.fields.filter(f => {return  f.props.name == key;})[0].props.multi : null) || filters[key].multiSelect || false;
            let modelKey = config[key].customKey || key;
            switch(config[key].type){
                case 'date':
                    let part = config[key].part || 'day';
                    array = array.filter((row) => evaluateDateCondition(row[modelKey],config[key].operator,filters[key],part, this.props.locale));
                    break;
                case 'freeText':
                    array = config[key].fuseOptions ?  new fuse(array,config[key].fuseOptions).search(filters[key]) : this.getDefaultFuseOptions(array,modelKey).search(filters[key]);
                    break;
                case 'boolean':
                    array = array.filter((row) => eval(filters[key]) == row[modelKey]);
                    break;
                case 'customFunction':
                    array = array.filter((row) => config[key].customFunction(row, filters[key]));
                    break;
                default:
                    if(Array.isArray(get(array[0],modelKey.split('.')[0]))){
                        let arrayName = modelKey.split('.')[0];
                        let criteria = modelKey.split('.').slice(1).join('.');
                        if(criteria){
                            array = isMultiSelect ? array.filter((row) => intersection(row[arrayName].map(element => get(element,criteria)),filters[key]).length > 0) :
                                array.filter((row) => row[arrayName].map(element => get(element,criteria)).includes(filters[key]));
                        }
                        else {
                            array = isMultiSelect ? array.filter((row) => intersection(row[arrayName].map(element => element),filters[key]).length > 0) :
                                array.filter((row) => row[arrayName].map(element => element).includes(filters[key]));
                        }

                    }
                    else{
                        array = isMultiSelect ? array.filter((row) => filters[key].includes(get(row,modelKey))) : array.filter((row) => get(row,modelKey) == filters[key]);
                    }
                    break;
            }
        }
        return array;
    }

    render() {
        return (
            <div className={`${this.state.open ? 'open' : ''}`}>
                <div className={this.props.containerClass}>
                    <form onSubmit={this.props.handleSubmit(this.handleApply.bind(this))} autoComplete={this.props.disableAutocomplete ? 'off' : null}>
                        <h5 className="title">
                            <span className="fa fa-filter" />
                            <strong> <FormattedMessage id="filters.title"/></strong>
                            { this.props.viewHideShowButtons ?
                                this.state.hidden?
                                    <button type="button" className={`btn-coloredborder ${this.props.userType}`} onClick={this.handleShowHide}><FormattedMessage id="filters.show"/></button>
                                    : <button type="button" className={`btn-coloredborder ${this.props.userType}`} onClick={this.handleShowHide}><FormattedMessage id="filters.hide"/></button>
                                :
                                null}
                            <div className={'title-buttons'}>
                              {
                                this.props.titleButtons ? this.props.titleButtons : null
                              }
                                { this.props.viewMoreButton ?
                                    <button type="button" className="text-button viewless" onClick={this.handleExpend}><span className="hover-icon" />{this.state.open ? <FormattedMessage id="components.commons.adistecFilters.viewMore"/> : <FormattedMessage id="components.commons.adistecFilters.viewLess"/>}</button>
                                    : null}
                                {!this.state.hidden?
                                    <button type="submit" className={`btn-coloredbg ${this.props.userType}`}><FormattedMessage id="filters.apply"/></button>
                                    :null}
                            </div>
                        </h5>
                        <div className={`filter-group ${this.state.open ? 'open' : ''} ${this.state.hidden ? 'hidden' : ''}`}>
                            <div className="row mb0">
                                {this.props.fields}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

AdistecFilters.propTypes = propTypes;
AdistecFilters.defaultProps = defaultProps;


function mapDispatchToProps(dispatch) {
    return({
        setFormTouched: (field) => dispatch(touch('filterForm',field))
    });
}

function mapStateToProps(state) {
    return {
        formValues: getFormValues('filterForm')(state) || {},
        getFormSyncErrors: getFormSyncErrors('filterForm')(state)
    };
}

AdistecFilters = connect(mapStateToProps,mapDispatchToProps)(AdistecFilters);

AdistecFilters = reduxForm({
    form:'filterForm'
})(AdistecFilters);

export default injectIntl(AdistecFilters);
