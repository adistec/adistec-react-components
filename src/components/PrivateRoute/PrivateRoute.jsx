import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import intersection from 'lodash/intersection';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            intersection(rest.userPermissions, rest.requiredPermissions).length > 0 ?
                (<Component {...props} />)
                :
                (<Redirect to={rest.redirectNotPermission}/>)
        }
    />
);


PrivateRoute.defaultProps =  {
    redirectNotPermission: '/'
};

PrivateRoute.propTypes = {
    redirectNotPermission: React.PropTypes.string,
    requiredPermissions:React.PropTypes.array.isRequired,
    userPermissions:React.PropTypes.array.isRequired
};

export default PrivateRoute;
