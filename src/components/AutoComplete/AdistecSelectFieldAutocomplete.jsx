import React from 'react';
import { connect } from 'react-redux';
import { Field,change,untouch } from 'redux-form';
import SelectAutoComplete from './AutoComplete.jsx';
import './AdistecSelectFieldAutocomplete.scss';
import { FormattedMessage } from 'react-intl';


const propTypes = {
    clearButton:React.PropTypes.boolean,
    disabled:React.PropTypes.boolean,
    items:React.PropTypes.array.isRequired,
    label:React.PropTypes.string,
    matchProp: React.PropTypes.string,
    maxSearchResults: React.PropTypes.number,
    name: React.PropTypes.string,
    noResultsText: React.PropTypes.object,
    onChange: React.PropTypes.func,
    onInputChange: React.PropTypes.func,
    userType: React.PropTypes.string,
    size:React.PropTypes.string,
    validate: React.PropTypes.array
};

const defaultProps = {
    noResultsText: <FormattedMessage id="autocomplete.noResultsFound"/>
};

class AdistecSelectFieldAutocomplete extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div className={this.props.size ? 'adistec-select-field-autocomplete col s' + this.props.size : ''} style={{ position: 'relative' }}>
                <Field component={SelectAutoComplete}
                       withRef={this.props.clearButton ? true : false}
                       ref={this.props.clearButton ? this.props.name : null}
                       name={this.props.name}
                       validate={this.props.validate}
                       items = {this.props.items}
                       placeholder = {this.props.label}
                       disabled={this.props.disabled}
                       maxSearchResults = {this.props.maxSearchResults}
                       noResultsText={this.props.noResultsText}
                       onChange={this.props.onChange}
                       onInputChange={this.props.onInputChange}
                       filterOption={this.props.filterOption}
                       matchProp={this.props.matchProp}
                       userType={this.props.userType}
                />
            </div>
        );
    }
}

AdistecSelectFieldAutocomplete.propTypes = propTypes;
AdistecSelectFieldAutocomplete.defaultProps = defaultProps;

export default connect(null, {
})(AdistecSelectFieldAutocomplete);
