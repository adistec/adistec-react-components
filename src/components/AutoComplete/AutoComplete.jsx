import React from 'react';
import SelectAutoComplete from 'react-select';

const propTypes = {
    disabled:React.PropTypes.boolean,
    filterOption: React.PropTypes.func,
    input:React.PropTypes.object,
    items:React.PropTypes.array.isRequired,
    matchProp: React.PropTypes.string,
    maxSearchResults: React.PropTypes.number,
    name: React.PropTypes.string.isRequired,
    noResultsText: React.PropTypes.object,
    onChange: React.PropTypes.func,
    onInputChange: React.PropTypes.func,
    placeholder: React.PropTypes.string,
    size:React.PropTypes.string
};

class AutoComplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectValue: this.props.input.value,
            inputValue: null
        };
    }


    componentWillReceiveProps(nextProps){
        if (this.state.selectValue!=nextProps.input.value){
            this.setState({
                selectValue: nextProps.input.value
            });
        }

    }

    onInputChange = (inputValue) =>{
        this.setState({
            inputValue: inputValue
        });
        this.props.onInputChange ? this.props.onInputChange(inputValue) : null;
    };


    onChange = (value) =>{
        this.props.input.onChange(value);
        this.setState({
            selectValue: value,
            inputValue: null
        });
        this.props.onChange ? this.props.onChange(value) : null;
    };

    onBlur = () =>{
        let value = this.state.inputValue;
        if (!this.props.items.filter(function (item) {return item.label === value;}).length > 0 && value) {
            this.props.input.onChange(null);
            this.setState({
                selectValue: null,
                inputValue: null
            });
        }
    };

    render() {
        return (
            <div>
                <SelectAutoComplete
                    onBlurResetsInput
                    onSelectResetsInput
                    options={this.props.items}
                    simpleValue
                    className={`
                        ${this.props.userType ? this.props.userType : 'adistec'}
                        ${this.props.meta.touched && this.props.meta.error ? 'error' : null}
                    `}
                    name={this.props.name}
                    clearable
                    disabled={this.props.disabled}
                    clearValueText=""
                    pageSize={this.props.maxSearchResults? this.props.maxSearchResults : 5 }
                    noResultsText={this.props.noResultsText}
                    value={this.state.selectValue}
                    placeholder={this.props.placeholder}
                    onInputChange={this.onInputChange.bind(this)}
                    onValueClick={this.onValueClick}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    openOnClick
                    searchable
                    filterOption={this.props.filterOption}
                    matchProp={this.props.matchProp}
                /><span className="helper-placeholder">{this.props.placeholder}</span>
                {this.props.meta.touched && this.props.meta.error ? <span className="error">{this.props.meta.error}</span> : null}
            </div>);
    }
}

AutoComplete.propTypes = propTypes;

export default AutoComplete;