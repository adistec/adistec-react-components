import React from 'react';
import DatePicker from 'material-ui/DatePicker';
import { Field, change , untouch , touch , stopAsyncValidation } from 'redux-form';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import DatePickerActions from './DatePickerActions.jsx';
import TextField from 'redux-form-material-ui/es/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import moment from 'moment';
import 'moment/src/locale/es';
import 'moment/src/locale/pt';

const propTypes = {
    clearButton: React.PropTypes.bool,
    defaultDate: React.PropTypes.object,
    disabled: React.PropTypes.bool,
    intl:React.PropTypes.object,
    label: React.PropTypes.string,
    name: React.PropTypes.string,
    onChange: React.PropTypes.function,
    size:React.PropTypes.array.string,
    validate:React.PropTypes.array
};

const bpodTheme = getMuiTheme({
    palette: { primary1Color: '#13017c' },
    datePicker: { selectColor: '#13017c' }
});

const adistecTheme = getMuiTheme({
    palette: { primary1Color: '#1E2CA2' },
    datePicker: { selectColor: '#1E2CA2' }
});

const textFieldStyle =  { width: '100% ', boxSizing: 'border-box ',height:'61px ',marginBottom: '10px', fontSize: '14px' };
const inputStyle = { marginTop: '22px',height:'50%', fontSize: '14px' };
const floatingLabelStyle =  { top: '45%', fontSize: '14px' };

class DatePickerWithInput extends React.Component {
    static contextTypes = {
        intl: React.PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            date: undefined
        };

        this.onBlurHandler = this.onBlurHandler.bind(this);
    }

    transferValue = (e, date) => {
        this.props.changeValue(this.getFormName(), this.props.name, date);
        this.props.onChange ? this.props.onChange(e, date) : null;
    } 

    getFormName = () => {
        return this.refs[this.props.name].getRenderedComponent().props.meta.form;
    }

    reset = (e) => {
        this.refs[this.props.name].getRenderedComponent().setState({ value:null });
        this.props.resetDatePicker(this.getFormName(),this.props.name,null);
        this.props.untouchDatePicker(this.getFormName(),this.props.name);
        this.props.onChange ? this.props.onChange(e, null) : null;
    }

    openDialog = () => {
        var calendarName = this.props.name + 'Calendar';
        this.refs[calendarName].openDialog();
    }

    onBlurHandler(event,value) {
        moment.locale(this.context.intl.locale);
        if(value && !moment(value,'L',true).isValid()){
            this.props.asyncValidation(this.getFormName(),{ [this.props.name]:this.context.intl.formatMessage({ id:'datepicker.incorrectDateFormat' }) });
        }
    }

    defaultFormat = (value,name) => {
        moment.locale(this.context.intl.locale);
        if(value){
            if(moment.isDate(value)){
                return moment(value).format('L');
            }
            else {
                return value;
            }
        }
        else{
            return '';
        }
    }

    defaultParse = (value) => {
        moment.locale(this.context.intl.locale);
        return moment(value,'L',true).isValid() ? moment(value,'L',true).toDate() : value;
    }

    render() {
        return (
            <div className={this.props.size ? 'col s' + this.props.size : ''} style={{ position:'relative' }}>
                <Field name={this.props.name} style={{ width: '100%',height:'61px' }} floatingLabelText={this.props.label}
                       withRef
                       ref={this.props.name}
                       component={TextField}
                       size={this.props.size}
                       disabled={this.props.disabled}
                       inputStyle={inputStyle}
                       format={this.props.format ? this.props.format : this.defaultFormat}
                       validate={this.props.validate}
                       onChange={this.props.onChange}
                       floatingLabelStyle={floatingLabelStyle}
                       className="adistec-date-picker"
                       parse={this.props.parse ? this.props.parse : this.defaultParse}
                       onBlur={this.onBlurHandler}
                       title={this.context.intl.formatMessage({ id:'datepicker.dateFormat' })}
                       floatingLabelFocusStyle={this.props.userType === 'bpod' ? { color: '#13017c' } : { color: '#1E2CA2' }}
                       underlineFocusStyle={this.props.userType === 'bpod' ? { borderColor: '#13017c' } : { borderColor: '#1E2CA2' }}
                />
                {!this.props.disabled ?
                 <span>
                    <DatePickerActions userType={this.props.userType} resetValue={this.reset} openDialog={this.openDialog} clearButton={this.props.clearButton}/>
                    <MuiThemeProvider muiTheme={this.props.userType ? bpodTheme : adistecTheme}>
                        <DatePicker name={this.props.name + 'Calendar'} 
                            ref={this.props.name + 'Calendar'} 
                            onChange={this.transferValue} 
                            textFieldStyle={textFieldStyle}
                            defaultDate={this.props.defaultDate? this.props.defaultDate : undefined}
                            style={{ 'display': 'none' }}
                            locale={this.context.intl.locale}
                            DateTimeFormat={Intl.DateTimeFormat}
                        />
                    </MuiThemeProvider>
                 </span>
                 : null}
            </div>
        );
    }
}

DatePickerWithInput.propTypes = propTypes;

DatePickerWithInput = connect(null, {
    resetDatePicker: (formName,fieldName,value) => change( formName, fieldName, value ),
    untouchDatePicker: (formName,fieldName) => untouch(formName,fieldName),
    changeValue: (formName, fieldName, value) => change (formName, fieldName, value),
    asyncValidation: (formName, errors) => stopAsyncValidation(formName, errors)
})(DatePickerWithInput);



export default injectIntl(DatePickerWithInput);