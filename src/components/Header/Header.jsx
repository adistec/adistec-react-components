import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import AccountMenu from '../AccountMenu/index.jsx';
import Drawer from '../Drawer/index.jsx';
import styled from 'styled-components';
import IconButton from 'material-ui/IconButton';
import Menu from 'material-ui/svg-icons/navigation/menu';
import './Header.scss';
import intersection from 'lodash/intersection';
import { injectIntl } from 'react-intl';

const HeaderMain = styled.header `
   padding: 14px 0px 15px 0px;
   z-index: 100;
   box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
`;
const HeaderContainer = styled.div `
   margin: 0 auto;
   max-width: 1280px;
   width: 90%;
   @media only screen and (min-width: 601px){
       width: 85%;
   }
`;
const AccountInfo = styled.div `
   float: right !important;
`;
const LogoStyle = styled.div `
   margin-right: 20px;
   float:left !important;
   a {
       width: 100%;
       text-align: center;
       height: 50px;
       display: block;
   }
`;
const LoginButton = styled(FlatButton)`
   background-color: #FAFAFA !important;
   box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
   top: 3px !important;
   color: var(--adistecBlue) !important;
   div > span {
       font-size: 17px !important;
       text-transform: none !important;
       font-weight: bold;
   }
`;
const LogoutButton = styled(FlatButton)`
   background-color: #FAFAFA !important;
   box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
   top: 3px !important;
   margin-left: 7px !important;
   color: var(--adistecBlue) !important;
   div > span {
       font-size: 17px !important;
       text-transform: none !important;
       font-weight: bold;
   }
`;
const AccountHeader = styled(FlatButton)`
   color: #FAFAFA !important;
   top: 3px !important;
   div > span {
       font-size: 17px !important;
       text-transform: none !important;
       font-weight: bold;
   }
`;
const MenuIcon = styled(IconButton)`
   float: left !important;
   padding: 0 !important;
   margin-right: 8px !important;
   svg {
       color: #FAFAFA !important;
       height: 34px !important;
       width: 34px !important;
   }
`;
const logoPropTypes = {
    logoSrc: React.PropTypes.string
};
const Logo = ({ logoSrc }) => <LogoStyle><a><img src={logoSrc} /></a></LogoStyle>;
Logo.propTypes = logoPropTypes;

const HeaderPropTypes = {
    apAddress: React.PropTypes.string,
    companyName: React.PropTypes.string,
    drawerItems: React.PropTypes.array,
    headerTheme: React.PropTypes.string,
    intl:React.PropTypes.object,
    logoSrc: React.PropTypes.string,
    menuTitle: React.PropTypes.string,
    permissions: React.PropTypes.array,
    userEmail: React.PropTypes.string,
    username: React.PropTypes.string,
};

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openAccount: false,
            openDrawer: false,
        };
    }

    static contextTypes = {
        intl: React.PropTypes.object.isRequired
    }

    toggleOpenAccount = (value) => {
        this.setState({ openAccount: value });
    };

    toggleOpenDrawer = (value) => {
        this.setState({ openDrawer: value });
    };

    filteredDrawerItems = () => {
        if(this.props.permissions){
            return this.props.drawerItems.filter(drawer => !drawer.permissions ||  intersection(this.props.permissions,drawer.permissions).length > 0);
        }
        else{
            return this.props.drawerItems;
        }
    };

    render() {
        let companyName = this.props.companyName ? this.props.companyName 
            : this.props.userType === 'bpod' ? 'BPOD LLC.': 'Adistec Corp.';
        
        return(
            <HeaderMain className={this.props.headerTheme}>
                <HeaderContainer>
                    {
                        this.props.drawerItems?
                            <div>
                                <Drawer open={this.state.openDrawer}
                                        onClose={() => this.toggleOpenDrawer(false)}
                                        items={this.filteredDrawerItems()}
                                        menuTitle={this.props.menuTitle}/>
                                <MenuIcon className="menu-icon"
                                          tooltipStyles={{ color: '#FAFAFA' }}
                                          onClick={() => this.toggleOpenDrawer(true)}
                                          tooltip={this.props.intl.formatMessage({ id: 'general.menu' })}>
                                            <Menu/>
                                </MenuIcon>
                            </div>
                            : null}
                    {this.props.logoSrc ? <Logo logoSrc={this.props.logoSrc} /> : null}
                    <div>
                        {this.props.userEmail && this.props.username ?
                            <AccountInfo>
                                <AccountHeader label={this.props.username}
                                               id="account-header"
                                               labelPosition="before"
                                               icon={<div>
                                                    <i className="material-icons">account_circle</i>
                                                    <strong>{companyName}</strong>
                                                </div>}
                                               onClick={() => this.toggleOpenAccount(true)}/>
                                {this.props.logoutClick ? <LogoutButton label={'Logout'} id="logout" onClick={this.props.logoutClick}/> : null}
                                {this.props.menuItems ?
                                    <AccountMenu userEmail={this.props.userEmail}
                                                 open={this.state.openAccount}
                                                 element={document.getElementById('account-header')}
                                                 onClose={() => this.toggleOpenAccount(false)}
                                                 menuItems={this.props.menuItems}/>
                                    : null}
                            </AccountInfo>
                            : this.props.loginClick ? <AccountInfo><LoginButton label={'Login'} id="login" onClick={this.props.loginClick} /></AccountInfo> : null}
                        {this.props.apAddress?
                            <div className="dashboard-button">
                                <a href={this.props.apAddress}><i className="material-icons">apps</i>
                                    <div className="cssTooltip">{this.context.intl.formatMessage({ id: 'dashboardIcon.dashboard' })}</div>
                                </a>
                            </div>
                            :null}
                    </div>
                </HeaderContainer>
            </HeaderMain>);
    }
};

Header.propTypes = HeaderPropTypes;

export default injectIntl(Header);