import React  from 'react';
import { FormattedMessage } from 'react-intl';
import isEmpty from 'lodash/isEmpty';
import isDate from 'lodash/isDate';

const required = (value) => {
    return ((typeof(value) == 'object' && !isDate(value) && isEmpty(value)) || (((typeof(value) != 'object' || isDate(value)) && !value)) ?
        <FormattedMessage id="validations.required"/> : undefined);
};

const email = (value)=>{
        return (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
            ? <FormattedMessage id="validations.email"/>
            : undefined);
};

const numbers = (value)=>{
    return (value && !/^[0-9]*$/g.test(value)
        ? <FormattedMessage id="validations.number"/>
        : undefined);
};

const notZero = (value)=>{
    return (value && /^[0]*$/g.test(value)
        ? <FormattedMessage id="validations.notZero"/>
        : undefined);
};

const date = (value)=>{
    return (!new Date(value) instanceof Date
        ? <FormattedMessage id="validations.date"/>
        : undefined);
};

const shortTime = (value)=>{
    return (value && !/^([01]{1}[0-9]|2[0-3]):[0-5][0-9]$/g.test(value)
        ? <FormattedMessage id="validations.shortTime"/>
        : undefined);
};

const dateFromAfterDateTo = (value, allValues, props) => {
    return (value && allValues.dateFrom && allValues.dateFrom > allValues.dateTo
        ? <FormattedMessage id="validations.dateFromAfterDateTo"/> : null);
};

const dateMMMYYYY = (value) => {
    return (value && !/((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d{4}$)/i.test(value)
        ? <FormattedMessage id="validations.dateMMMYYYY"/>
        : undefined);
    };

const numbersGroup = (value)=>{
    return (value && !/(\d+)/.test(value)
        ? <FormattedMessage id="validations.numbersGroup"/>
        : undefined);
};

const upperCaseGroup = (value)=>{
    return (value && !/([A-Z]+)/.test(value)
        ? <FormattedMessage id="validations.upperCaseGroup"/>
        : undefined);
};

const lowerCaseGroup = (value)=>{
    return (value && !/([a-z]+)/.test(value)
        ? <FormattedMessage id="validations.lowerCaseGroup"/>
        : undefined);
};

const notAlphanumericsGroup = (value) => {
        return (value && !/(\W+)/.test(value)
            ? <FormattedMessage id="validations.notAlphanumericsGroup"/>
            : undefined);
};


const validationsInCharactersGroups = (validationsGroups, numMinValidGroups) => {
    return (value) => {
        let wrongGroups = []; //Lista de errores en los grupos no coincidentes
        let validGroups = 0; // Cantidad de grupos coincidentes
        validationsGroups.map(vg => {return !vg(value) ? validGroups++ : wrongGroups.push(vg(value));});
        return (value && validGroups < numMinValidGroups)
            ? <div> <FormattedMessage id="validations.inCharactersGroups"
                                      values={{ numMinValidGroups: numMinValidGroups - validGroups, totalGroups: wrongGroups.length }}/> {wrongGroups.map(wq => {return <span>{wq} </span>;})} </div>
            :  undefined;
    };
};

const atLeastXCharacters = (x)  => {
    var exp = new RegExp('(^.{'+x+',}$)');
    return (value) => {
        return (!value || !exp.test(value)
            ? <FormattedMessage id="validations.atLeastXCharacters" values={ { x: x } }/>
            : undefined);
    };
};

const maxLength = (value) => {
    let maxCharAllowed = 250;
    return value && value.length > maxCharAllowed ?
        <FormattedMessage id="validations.maxLength" />
        : undefined;
};

export default { maxLength, validationsInCharactersGroups, required , email , numbers ,notZero , date,shortTime ,dateFromAfterDateTo ,
    dateMMMYYYY, numbersGroup, lowerCaseGroup, upperCaseGroup, notAlphanumericsGroup, atLeastXCharacters };
