import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'react-bootstrap';
import './Breadcrumb.scss';
import styled from 'styled-components';
import find from 'lodash/find';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { matchPath } from 'react-router';

const propTypes = {
    /**
     * Contenido que se incluirá inmediatamente después del componente "Breadcrumbs"
     * Puede utilizarse para mostrar, por ejemplo, un botón (<button>) o un mensaje (<span>)
     */
    appendedContent:PropTypes.any,
    /**
     * URL actual
     */
    currentPath: PropTypes.string.isRequired,
    /**
     * Array de objetos. Debe ser del tipo [{label: string, path: string, notRedirect: boolean, customLabel: function}]
     * donde path es la ruta del objeto, label es el nombre del objeto, notRedirect especifica si el objeto es redireccionable o no y customLabel es una funcion
     * para obtener el label de los pathVariables pasado por parametros de la URL a resolver
     */
    routes: PropTypes.array.isRequired,
    /**
     * Simbolo para separar una ruta de otra
     */
    separator:PropTypes.string,
};

const Breadcrumbs = styled(Breadcrumb)`
   .breadcrumbs-container .breadcrumb > li + li:before {
       content: ${props=>props.styles.separator} !important;
    }
`;


class AdistecBreadcrumb extends React.Component {
    constructor(props) {
        super(props);
    }

    renderItems = () => {
        let currentPathItems = this.props.currentPath === '/' ? [''] : this.props.currentPath.split('/');
        let fullPathMobile = '/';
        let fullPath = '/';

        return currentPathItems.map((path, index) => {
            fullPathMobile += index > 1 ? '/' + path : path;
            let item = find(this.props.routes, function(route) {return matchPath(fullPathMobile, route.path) && matchPath(fullPathMobile, route.path).isExact;});
            let label = item && item.label? item.label : (item.customLabel ? item.customLabel(matchPath(fullPathMobile, item.path).params) : path);
            return (<Breadcrumb.Item key={index} active as={'div'}>
                {item?
                    item.notRedirect || index === (currentPathItems.length -1) ? //Se muestra como Read Only. No se puede redirigir
                        <span> {label} </span>
                        : <Link to={fullPathMobile}> {label} </Link>
                    :  <span> {label} </span>}
            </Breadcrumb.Item>);
        });
    }

    render() {

        return (
            <div className="breadcrumbs-container">
                <div className="container flex-space-between flex-center">
                    <Breadcrumbs styles={ { separator: this.props.separator } }>
                        <li className="breadcrumb-mobile">
                            <input id="breadcrumb-mobile-enabled" type="checkbox" />
                            <label htmlFor="breadcrumb-mobile-enabled">
                                <span className="material-icons">arrow_drop_down</span>
                            </label>
                            <ol>
                                {this.renderItems()}
                            </ol>
                        </li>
                            {this.renderItems()}
                    </Breadcrumbs>
                    {this.props.appendedContent}
                </div>
            </div>
        );
    }
}

AdistecBreadcrumb.propTypes = propTypes;

export default AdistecBreadcrumb;