import React from 'react';
import Dropzone from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { stopAsyncValidation ,clearAsyncError ,untouch } from 'redux-form';
import compact from 'lodash/compact';
import isEmpty from 'lodash/isEmpty';


const formatBytes = (bytes,decimals) => {
    if(bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

const defaultProps = {
    multiple: true
};

const DropZoneElement = (props) => {
    const buildUrl = (file) => {
        let url = props.directory ? props.directory + file.fileLink : file.fileLink;
        return url;
    };

    const fileTypesToDisplay = ['jpg', 'jpeg', 'gif', 'png'];

    const buildUrlFile = (file) => {
        return props.directory ? props.directory + file.fileLink : file.fileLink;
    };

    const displayContent = (urlFile) => {
        let extensionFyle = urlFile.split('.').pop();
        return fileTypesToDisplay.includes(extensionFyle);
    };

    const getFilenameOnly = (urlFile) => {
        let fileName = urlFile.substring(urlFile.lastIndexOf('/')+1);
        return fileName;
    };

    return (
        props.multiple ?
            props.file.fileToUpload ?
                <li style={{ zIndex:100 }}>
                    <span className="file-name">{props.file.fileToUpload.name}</span>
                    <span className="file-size">{formatBytes(props.file.fileToUpload.size)}</span>
                    <span className="file-deleteIcon" onClick={(event) => props.deleteFile(event,props.index)}><i className="fa fa-times" aria-hidden="true"/></span>
                </li> :
                <li style={{ zIndex:100 }}>
                    <span className="file-name">{props.file.fileLink.split('/')[props.file.fileLink.split('/').length - 1]}</span>
                    <a className="file-downloadIcon" href={buildUrlFile(props.file)}><span onClick={event => event.stopPropagation()}><i className="fa fa-download" /></span></a>
                    <span className="file-deleteIcon" onClick={(event) => props.deleteFile(event,props.index)}><i className="fa fa-times" aria-hidden="true"/></span>
                </li>
            :
            props.file.fileToUpload ?
                <li style={{ zIndex:100 }}>
                    {displayContent(props.file.fileToUpload.name) ?
                        <img src={props.file.fileToUpload.preview}
                             style={{ maxHeight:'100px' }} />
                        : <span className="preview-filename"><i className="fa fa-file-o lg-size" /><strong>{props.file.fileToUpload.name}</strong></span>
                    }
                    <span className="file-deleteIcon" style={{ zIndex:9 }} onClick={(event) => props.deleteFile(event,props.index)}><i className="fa fa-times" aria-hidden="true"/></span>
                </li> :
                <li style={{ zIndex:100 }}>
                    {displayContent(props.file.fileLink) ?
                        <img src={buildUrl(props.file)}
                             style={{ maxHeight:'100px' }}/>
                        : <span className="preview-filename"><i className="fa fa-file-o lg-size" /><strong>{getFilenameOnly(props.file.fileLink)}</strong></span>
                    }
                    {props.showDownloadButton ? <a className="file-downloadIcon" href={buildUrl(props.file)}><span onClick={event => event.stopPropagation()}><i className="fa fa-download" /></span></a> : null}
                    <span className="file-deleteIcon" style={{ zIndex:9 }} onClick={(event) => props.deleteFile(event,props.index)}><i className="fa fa-times" aria-hidden="true"/></span>
                </li>

    );
};


const DropZoneList = (props) => {
    return (
        <div>
            {props.files.length > 0 ?
                <ul className={props.multiple ? 'multiple-file' : 'single-file'}>
                    {props.files.map((file,index) =>
                        <DropZoneElement file={file} index={index} {...props}/>
                    )}
                </ul> :
                <div>
                    <strong>{props.label}</strong>
                    <span className="hint">{props.hint}</span>
                </div>}
        </div>
    );
};



const ErrorBlock = (props) => {
    return (
        props.showError || (props.meta.touched && props.meta.error) ?
            <div className="error-message">
                {
                    !isEmpty(props.internalErrors) ?
                        <span className="message-text">{props.internalErrors[0]}</span> : <span className="message-text">{props.meta.error}</span>
                }
                <span className="message-closeIcon" onClick={(event) => props.closeError(event)}><i className="fa fa-times" aria-hidden="true"/></span>
            </div>
            : null
    );
};

class DropZoneInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: this.props.input.value ? this.getFiles(this.props.input.value) : [],
            showError: false,
            errorFileType: false,
            directory: this.props.directory,
            asyncError: undefined
        };
    }


    componentWillReceiveProps(nextProps){
        if(this.props.multiple){
            if (nextProps.input.value != this.state.files){
                if(nextProps.input.value){
                    let files = this.state.files.slice();
                    files = files.concat(nextProps.input.value.map(file => ({ 'fileLink':file })));
                    this.setState({
                        files:files
                    });
                }
                else{
                    this.setState({ files:[] });
                }
            }
        }
        else{
            if (nextProps.input.value != this.state.files[0]){
                if(nextProps.input.value){
                    let files;
                    if(Array.isArray(nextProps.input.value)){
                        //primer caso se cargo en el value , segundo caso lo actualizo el propio form
                        if(nextProps.input.value[0]){
                            {files = nextProps.input.value[0].fileLink ? [{ 'fileLink':nextProps.input.value[0].fileLink,'file':nextProps.value[0].file }] : [{ 'fileLink':nextProps.input.value[0] }];}
                        }
                        else {
                            files = [];
                        }
                    }
                    else
                    {files = nextProps.input.value.fileLink ? [{ 'fileLink':nextProps.input.value.fileLink,'file':nextProps.value.file }] : [{ 'fileLink':nextProps.input.value }];}
                    this.setState({ files:files });
                }
                else {
                    this.setState({ files:[] });
                }
            }
        }
    }

    getFiles = (value) => {
        if(Array.isArray(this.props.input.value)){
            if(this.props.multiple){
                return this.props.input.value.map(file => ({ 'fileLink':file }));
            }
            else{
                return [{ 'fileLink':this.props.input.value[0] }];
            }
        }
        else{
            return [{ 'fileLink':this.props.input.value }];
        }
    }

    getFormName = () => {
        return this.props.meta.form;
    }

    updateFilesArrayState = (fileToUpload) => {
        let files;
        if(this.props.multiple){
            files = this.state.files.slice().concat(fileToUpload.map(file => ({ fileToUpload:file,fileLink:file.preview })));
            this.setState({ files: files, directory: null,showError:false,errorFileType:undefined }, () => {
                this.props.input.onChange(files);});
        }
        else{
            files = [{ fileToUpload:fileToUpload[0],fileLink:fileToUpload[0].preview }];
            this.setState({ files: files, directory: null,showError:false,errorFileType:undefined }, () => {
                this.props.input.onChange(files[0]);});
        }
    }

    onDropAccepted = (fileToUpload) => {
        if(this.props.afterDrop){
            this.props.afterDrop(fileToUpload).then((response) => {
                if(response){
                    this.setState({ showError:true,asyncError: response });
                    this.props.asyncValidation(this.getFormName(),{ [this.props.input.name]:response });
                }
                else{
                    this.updateFilesArrayState(fileToUpload);
                }
            });
        }else{
            this.updateFilesArrayState(fileToUpload);
        }
    }

    onDropRejected = (fileToUpload) => {
        this.setState({ showError:true,errorFileType:<FormattedMessage id="uploadFile.fileError" values={{ allowedFileTypes: this.props.allowedFileTypes, maxSize: formatBytes(this.props.maxSize) }}/> });
        this.props.asyncValidation(this.getFormName(),{ [this.props.input.name]:<FormattedMessage id="uploadFile.fileError" values={{ allowedFileTypes: this.props.allowedFileTypes, maxSize: formatBytes(this.props.maxSize) }}/> });
    }


    deleteFile = (event,index) => {
        let files = this.state.files.slice();
        files.splice(index,1);
        this.setState({ files:files },() => {this.props.input.onChange(files);});
        if(files.length == 0){
            this.props.untouch();
        }
        event.stopPropagation();
    }

    closeError = (event) => {
        this.setState({ showError:false });
        this.props.untouch(this.getFormName(),this.props.input.name);
        this.props.clearAsyncError(this.getFormName(),this.props.input.name);
        event.stopPropagation();
    }

    render() {
        return (
            <div className="dropZoneContainer">
                <ErrorBlock showError={this.state.showError}
                            internalErrors={compact([this.state.errorFileType,this.state.asyncError])}
                            meta={this.props.meta}
                            closeError={this.closeError.bind(this)}/>
                <Dropzone
                    accept={this.props.allowedFileTypes}
                    maxSize={this.props.maxSize}
                    onDropAccepted={(filesToUpload) => this.onDropAccepted(filesToUpload)}
                    onDropRejected={(filesToUpload) => this.onDropRejected(filesToUpload)}
                    onClick={event=> event.preventDefault()}
                    multiple={this.props.multiple}
                    disabled={this.state.showError}
                    className="dropZoneElement"
                >
                    <DropZoneList
                        hint={this.props.hint}
                        label={this.props.label}
                        files={this.state.files}
                        showDownloadButton={this.props.showDownloadButton}
                        deleteFile={this.deleteFile.bind(this)}
                        directory={this.props.directory}
                        multiple={this.props.multiple}
                    />
                </Dropzone>

            </div>);
    }
}

DropZoneInput.defaultProps = defaultProps;


DropZoneInput = connect(null, {
    asyncValidation: (formName, errors) => stopAsyncValidation(formName, errors),
    untouch: (formName,...fields) => untouch(formName,...fields),
    clearAsyncError: (formName,...fields) => clearAsyncError(formName,...fields)
})(DropZoneInput);

export default DropZoneInput;