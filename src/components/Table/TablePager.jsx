import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import range from 'lodash/range';

const propTypes = {
    initialPage: React.PropTypes.number,
    items: React.PropTypes.array.isRequired,
    onChangePage: React.PropTypes.func.isRequired,
    pageSize:React.PropTypes.number,
    pagerContianerClass:React.PropTypes.string,
    renderCurrentPage:React.PropTypes.bool,
    intl:React.PropTypes.object,
};

const defaultProps = {
    initialPage: 1,
    pageSizeOptions: [10,20,50,100]
};

class TablePager extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pager: {
            pageSize: this.props.pageSize
        } };
    }

    static contextTypes = {
        intl: React.PropTypes.object.isRequired
    }

    componentWillMount() {
        // set page if items array isn't empty
        if (this.props.items && this.props.items.length) {
            this.setPage(this.props.initialPage);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // reset page if items array has changed
        if (this.props.items !== prevProps.items) {
            if (this.props.renderCurrentPage){
                this.setPage(this.state.pager.startIndex == this.state.pager.endIndex && this.state.pager.currentPage>1 ? this.state.pager.currentPage -1 : this.state.pager.currentPage,true);
            }else{
                this.setPage(this.props.initialPage,true);
            }
        }
        if(this.state.pager.pageSize !== prevState.pager.pageSize){
            this.setPage(this.props.initialPage);
        }
    }

    setPage(page,forceChange) {
        var items = this.props.items;
        var pager = this.state.pager;

        if ((page < 1 || page > pager.totalPages) && !forceChange) {
            return;
        }

        // get new pager object for specified page
        pager = this.getPager(items.length, page, pager.pageSize);

        // get new page of items from items array
        var pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

        // update state
        this.setState({ pager: pager });

        // call change page function in parent component
        this.props.onChangePage(pageOfItems);
    }

    getPager(totalItems, currentPage, pageSize) {
        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);

        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        var pages = range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    handleChange = (event) => {
        var newPagerState = Object.assign({}, this.state.pager);
        newPagerState.pageSize = Number.parseInt(event.target.value);
        this.setState({ pager: newPagerState });
    }

    render() {
        var pager = this.state.pager;

        if (!pager.pages || pager.totalItems < defaultProps.pageSizeOptions.sort(function(a, b) {return a - b;})[0]) {
            return null;
        }

        return (
            <div className={this.props.pagerContianerClass}>
                <div className="text-left pagesize">
                    <label><FormattedMessage id="table.tablePager.itemsPerPage"/></label>
                    <select type="text" placeholder="Type" onChange={this.handleChange} value={pager.pageSize}>
                        {defaultProps.pageSizeOptions.sort(function(a, b) {return a - b;}).map((pageSize,key) => <option key={key} value={pageSize}>{pageSize} </option>)}
                    </select>
                </div>
                <div className="text-right pager">
                    {`${pager.startIndex + 1} - ${pager.endIndex + 1} ${this.context.intl.formatMessage({ id:'table.tablePager.of' })} ${pager.totalItems}` }
                    <button 
                        className="pager-prev" 
                        onClick={() => this.setPage(pager.currentPage - 1)}
                        style={
                            pager.startIndex === 0 ? 
                            { opacity: 0.3, cursor: 'initial' } : null
                        }
                    >
                        <span className="fa fa-chevron-left" />
                    </button>
                    <button 
                        className="pager-next"
                        onClick={() => this.setPage(pager.currentPage + 1)}
                        style={
                            (pager.endIndex + 1) === pager.totalItems ? 
                            { opacity: 0.3, cursor: 'initial' } : null
                        }
                    >
                        <span className="fa fa-chevron-right" />
                    </button>
                </div>
            </div>


        );
    }
}

TablePager.propTypes = propTypes;
TablePager.defaultProps = defaultProps;

export default TablePager;