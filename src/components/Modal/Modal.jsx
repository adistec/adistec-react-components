import React from 'react';
import Dialog from 'material-ui/Dialog';
import styled from 'styled-components';
import Button from '../Button/index.jsx';
import './Modal.scss';


const StyledModal = styled(Dialog)`
    padding-top: ${props=>props.className.maxHeight} !important;
    @media (max-height: 667px) {
        padding-top: ${props=>props.className.height} !important;
    }
    
    &>div > div > div { 
        padding-top: 16px !important;
    }
`;


const propTypes = {
    actions: React.PropTypes.array,
    bodyClass: React.PropTypes.string,
    content: React.PropTypes.object,
    height: React.PropTypes.string,
    maxHeight: React.PropTypes.string,
    maxWidth: React.PropTypes.string,
    open: React.PropTypes.bool,
    title: React.PropTypes.string,
    width: React.PropTypes.string
};

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="successModal" name="successModal">
                <StyledModal
                    actions={this.props.actions}
                    modal={this.props.onRequestClose ? false : true}
                    open={this.props.open}
                    className={{ height: this.props.height, maxHeight: this.props.maxHeight }}
                    contentClassName={this.props.contentClassName ? this.props.contentClassName : 'success-modal'}
                    bodyClassName={this.props.bodyClass}
                    bodyStyle={{ maxHeight: 'inherit', border: 0 }}
                    title={<h4 className="section-title light">{this.props.title}</h4>}
                    titleStyle={{ 
                        margin: '0px', 
                        padding: '0px 25px 15px 25px', 
                        color: this.props.userType === 'bpod' ? '#13017c':'#1E2CA2' }}
                    autoDetectWindowHeight
                    autoScrollBodyContent
                    onRequestClose={this.props.onRequestClose}
                    actionsContainerStyle={{ height: '68px', paddingRight: '16px' }}
                    contentStyle={{ maxHeight: 'inherit', width: this.props.width, maxWidth: 'none' }}>
                    <div id="content">
                        {this.props.content ? this.props.content : null }
                    </div>
                </StyledModal>
            </div>
        );
    }
}

Modal.propTypes = propTypes;

export default Modal;