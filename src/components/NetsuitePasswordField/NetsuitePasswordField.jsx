import React from 'react';
import GeneralValidations from '../GeneralValidations/index.jsx';
import PasswordField from '../PasswordField/AdistecPassword.jsx';
import { injectIntl } from 'react-intl';

const validations = [GeneralValidations.validationsInCharactersGroups(
                        [GeneralValidations.numbersGroup, GeneralValidations.lowerCaseGroup,
                            GeneralValidations.upperCaseGroup, GeneralValidations.notAlphanumericsGroup], 3),
                        GeneralValidations.atLeastXCharacters(10)];

const propTypes = {
    clearButton: React.PropTypes.bool,
    label: React.PropTypes.string,
    name: React.PropTypes.string,
    validate: React.PropTypes.func
};

class NetsuitePasswordField extends React.Component {

    static contextTypes = {
        intl: React.PropTypes.object.isRequired
    }

    render() {
        return (
            <div>
                <PasswordField
                    clearButton={this.props.clearButton}
                    helpTooltip={this.context.intl.formatMessage({ id:'validations.netSuitePassword' })}
                    name={this.props.name}
                    label={this.props.label}
                    userType={this.props.userType}
                    size="12 m6 l6"
                    showSecurityLevelBar
                    validate={this.props.validate ? validations.concat(this.props.validate) : validations}
                />
            </div>
        );
    }
}

NetsuitePasswordField.propTypes = propTypes;

export default injectIntl(NetsuitePasswordField);


