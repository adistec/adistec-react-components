import React from 'react';
import  Toggle  from 'redux-form-material-ui/es/Toggle';
import { Field } from 'redux-form';

const propTypes = {
    defaultChecked: React.PropTypes.bool,
    marginTop:React.PropTypes.string,
    name: React.PropTypes.string,
    size:React.PropTypes.string,
    title: React.PropTypes.string,
    titlePosition:React.PropTypes.string
};

const AdistecSwitch = ({
    name,
    title,
    defaultChecked,
    titlePosition,
    size,
    marginTop,
    disabled,
    userType,
    width
}) => (
    <div className={size ? 'col s' + size : ''}  style={{ marginTop: marginTop, width: width ? width:'' }}>
        <Field  defaultToggled={defaultChecked}
                label={title}
                component={Toggle}
                name={name}
                labelPosition={titlePosition}
                thumbSwitchedStyle={
                    userType === 'bpod' ? 
                        { backgroundColor: '#13017c' } : { backgroundColor: '#1E2CA2' }
                }
                trackSwitchedStyle={
                    userType === 'bpod' ? 
                        { backgroundColor: 'rgba(19, 1, 124, 0.5)' } : { backgroundColor: 'rgba(30,44,162,0.5)' }
                }
                disabled={disabled}/>
    </div>
);

AdistecSwitch.propTypes = propTypes;

export default AdistecSwitch;