import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import { Link } from 'react-router-dom';
import './Drawer.scss';

const propTypes = {
    items: React.PropTypes.array,
    menuTitle: React.PropTypes.string,
    onClose: React.PropTypes.func,
    open: React.PropTypes.func
};


const AdistecDrawer = ({
    open,
    onClose,
    items,
    menuTitle
}) => (
    <Drawer open={open}
            docked={false}
            onRequestChange={onClose}
            className={open ? 'adistec-drawer open' : 'adistec-drawer closed'}
            >
        <MenuItem primaryText={menuTitle} disabled/>
        {
            items.map((i,k)=>{
                return (
                    i.subheader ?
                        <Subheader key={k} style={{ borderTop:'solid 1px #DDD',cursor:'default' }}>{i.label}</Subheader>
                        :
                        <Link key={k} to={`/${i.path}`} onClick={(e) => {i.onClick && i.onClick(e); onClose(e);}}>
                            <MenuItem primaryText={i.label} style={i.subitem ? { paddingLeft:'15px' } : null}/>
                        </Link>
                );
            })
        }
        <Divider/>
    </Drawer>
);

AdistecDrawer.propTypes = propTypes;

export default AdistecDrawer;