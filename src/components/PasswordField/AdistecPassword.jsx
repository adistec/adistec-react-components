import React from 'react';
import { Field,change,untouch } from 'redux-form';
import AdistecTextField from '../TextField/TextField.jsx';
import './Password.scss';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import isEmpty from 'lodash/isEmpty';
import filter from 'lodash/filter';


const propTypes = {
    clearButton: React.PropTypes.bool,
    helpTooltip:React.PropTypes.string,
    label:React.PropTypes.string,
    name:React.PropTypes.string.isRequired,
    showSecurityLevelBar: React.PropTypes.bool,
    size: React.PropTypes.string,
    validate: React.PropTypes.array
};

const defaultProps = {
    showSecurityLevelBar: false,
};

class AdistecPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: null,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.setState({
            inputValue: value
        });
    }

    levelsMarkerNode = () => {
        let markerArray = [];
        if(this.refs[this.props.name + 'wrapper']){
            let markerClassName = '';
            const levelsLength = this.props.validate.length;  
            let formName = this.refs[this.props.name + 'wrapper'].getWrappedInstance().refs[this.props.name].context._reduxForm.form;           
            this.props.validate.map((item, index) => {
                let markerClassName = '';
                let allFormValues = this.props.getFormValues(formName);   
                const levelsLength = this.props.validate.length;
                const levelsValidLength = this.props.validate.filter((validation) => {return !validation(this.state.inputValue,allFormValues);}).length;
                if (this.state.inputValue !== null && this.state.inputValue !== '') {
                    switch (true) {
                        case levelsLength === levelsValidLength:
                            markerClassName = 'success';
                            break;
                        case levelsValidLength === 1 && index === 0:
                            markerClassName = 'danger';
                            break;

                        case levelsValidLength > 1 && index < levelsValidLength:
                            markerClassName = 'warning';
                            break;

                        default:
                            markerClassName = 'none';
                            break;
                    }
                }else{
                    markerClassName = 'none';
                }

                markerArray.push(<div className={markerClassName} key={`marker-${index}`} />);
            });
                
        }
        return markerArray;      
    };

    render() {
        return (
            <div className={`adistec-password-container col s${this.props.size}`}>
                {this.props.helpTooltip ? <i title={this.props.helpTooltip} className="material-icons">help</i> : null }
                <AdistecTextField
                    name={this.props.name}
                    type="password"
                    ref={this.props.name + 'wrapper'}
                    size="12 m6 l6"
                    onChange={(e, value) => this.handleChange(value)}
                    label={this.props.label}
                    userType={this.props.userType}
                    validate={this.props.validate}
                    clearButton={this.props.clearButton} />
                {this.props.validate.length > 0 && this.props.showSecurityLevelBar ? (
                    <div className="input-password__level">
                        <div className="input-password__marker">
                            {this.levelsMarkerNode()}
                        </div>
                    </div>
                ) : null }
            </div>
        );
    }
}

AdistecPassword.propTypes = propTypes;
AdistecPassword.defaultProps = defaultProps;


export default connect(state => ({
    getFormValues: (formName) => getFormValues(formName)(state)
}))(AdistecPassword);


