import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const propTypes = {
  containerStyle: React.PropTypes.string,
  imgStyle: React.PropTypes.string,
  size: React.PropTypes.string
};


const Loader = ({ containerStyle,imgStyle, size, userType }) => (
    <div style={containerStyle ? containerStyle : { mixBlendMode:'multiply', zIndex:'1000',textAlign:'center',position: 'absolute',top:'50%',transform:'translate(-50%,-50%)',left: '50%' }} className="loader-container">
        <CircularProgress size={size} color={userType ? '#13017c':'#1E2CA2'} />
    </div>
);

Loader.propTypes = propTypes;

  export default Loader;