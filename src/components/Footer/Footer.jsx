import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import './Footer.scss';

const Footer = ({companyName}) => (
    <footer>
        <p>Copyright © {(new Date()).getFullYear()} {companyName ? companyName : 'Adistec Corp.'} <FormattedMessage id="general.allRightReserved" /></p>
    </footer>
);

export default Footer;