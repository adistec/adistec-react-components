import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider,addLocaleData } from 'react-intl';
import { translatedMessages, mergeTranslations } from '../../config/translation';

const propTypes = {
    locale: React.PropTypes.string, // [en:{},es:{},pt:{}]
    messages: React.PropTypes.array
};

class IntlProviderWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locale: props.locale ? props.locale : 'en',
            messages:null
        };
    }

    componentWillMount() {
      const messages = mergeTranslations(this.props.messages);
      this.setState({ messages:messages });
      this.tryToAddLocaleData(messages);
    }

    tryToAddLocaleData(messages){
        let localeArray = [];
        Object.keys(messages).map((locale) => {
            let localePkg = require('react-intl/locale-data/' + locale);
            if(localePkg){
                localeArray.push(...localePkg);
            }
        });
        addLocaleData(localeArray);
    }

    changeLocaleState = (locale) => {
        if(this.state.locale != locale){
            this.setState({ locale:locale });
        }
    }

    render(){
        return (
            this.state.messages ? 
            <IntlProvider locale={this.state.locale} messages={translatedMessages(this.state.locale,this.state.messages)} children={this.props.children} /> : null
        );
    }
}


IntlProviderWrapper.propTypes = propTypes;

export default IntlProviderWrapper;