import React from 'react';
import { Route, Switch } from 'react-router-dom';
import List from './List';
import IntranetForm from './IntranetForm';
import Form from './Form';
import { Tabs, Tab } from 'material-ui/Tabs';
import FeedbackButton from '../../src/components/FeedbackButton';
import Header from '../../src/components/Header';
import Footer from '../../src/components/Footer';
import Banner from '../../src/components/Banner';
import ProgressKendo from './ProgressKendo';
import AdistecBreadcrumb from '../../src/components/Breadcrumb';
import routes from '../data/routes';
import { currentUserType } from '../data/userType';

const items = [
    { label: 'Examples', path: '' },
    { label: 'Subheader Example', subheader:true },
    { label: 'Form', path: '', subitem:true },
    { label: 'AdminExamples', path: '', permissions:['ADMIN_USER'] }
];

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }

    toggleDrawer = () => {
        this.setState({ open: !this.state.open });
    };

    getBannerItems = () =>{
        return [<div><a href="https://www.fortinet.com" target="_blank"><img src="/examples/assets/img/FORTINET.png"/></a></div>,
                     <div><a href="https://www.kaspersky.es" target="_blank"><img src="/examples/assets/img/KASPERSKY.png"/></a></div>];
    };

    getBannerSetting = () => {
    return  {
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows:false,
        infinite: true,
        speed: 500,
        slidesToShow: 1
        };

     }

    render() {
        // console.log(this.props)
        return (
            <div className="appWrapper">
                <Switch>
                    <Route path="/cualquiercosa" component={()=><div>Ejemplo ruta</div>}/>
                </Switch>
                <Header
                userType={currentUserType}
                        toggleDrawer={this.toggleDrawer}
                        drawerOpen={this.state.open}
                        drawerItems={items}
                        menuTitle="Adistec Components"
                        username={'Logged User Name'}
                        accountOpen={false}
                        permissions={['ADISTEC_REACT_COMPONENTS_USER']}/>
                <div id="main-wrapper">
                    <AdistecBreadcrumb 
                        routes={routes.map(r => {
                            return { path: r.path, label: r.breadcrumb.label };
                        })}
                        currentPath={this.props.location.pathname}
                        style={{ position: 'static' }}
                    />
                    <br/>
                    <div className="container">
                        <Banner items={this.getBannerItems()} setting={this.getBannerSetting()}/>
                        <Tabs>
                             <Tab label="Example 1: Form">
                                 <Form/>
                             </Tab>
                            <Tab label="Example 2: List">
                                <List/>
                            </Tab>
                            <Tab label="Example 3: Intranet Form">
                                <IntranetForm/>
                            </Tab>
                            <Tab label="Example 4: Progress Kendo Components">
                                <ProgressKendo/>
                            </Tab>
                        </Tabs>
                        <FeedbackButton placeholder={'Tell us what you think of Adistec Components'} handleSubmit={() => Promise.resolve()}/>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}


export default Main;