import React from 'react';
import {
    change,
    Field,
    getFormSyncErrors,
    getFormValues,
    isValid,
    reduxForm,
    reset,
    stopSubmit,
    submit,
    touch,
    validate,
    initialize
} from 'redux-form';
import { connect } from 'react-redux';
import TextField from '../../src/components/TextField';
import SelectField from '../../src/components/SelectField';
import Button from '../../src/components/Button';
import Modal from '../../src/components/Modal';
import HtmlTextEditor from '../../src/components/HtmlTextEditor';
import UploadFile from '../../src/components/UploadFile';
import Switch from '../../src/components/Switch';
import Autocomplete from '../../src/components/AutoComplete';
import GeneralValidations from '../../src/components/GeneralValidations';
import NetsuitePasswordField from '../../src/components/NetsuitePasswordField';
import DatePickerWithInput from '../../src/components/DatePickerWithInput';
import { FormattedMessage } from 'react-intl';
import { currentUserType } from '../data/userType';

const rePassword = (value,allValues)=>{
        return (value && allValues.password && value != allValues.password
            ? <FormattedMessage id="form.rePassword"/>
            : undefined);
};

const propTypes = {
    handleSubmit: React.PropTypes.func.isRequired
};


class Form extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showSuccessRegisterMessage: false,
            registerMessage: null
        };
    }

    getSexList = () => {return [
        { label: 'Male', value:1 },
        { label: 'Female',value:2 },
        { label: 'Other',value:3 },
        { label: 'Unspecified',value:4 },
        ]; };

    componentWillMount() {
      setTimeout(this.initializeForm,5000);
    }

    initializeForm = () => {
      this.props.initialize({ 'photo':['https://www.adistec.com/Admin/RequestUploads/ADISTEC[1]_10_08_2018.zip'] });
    }

    handleSubmit(data) {
        this.setState({ showSuccessRegisterMessage: true,
                      registerMessage:
                          <div className="row" style={{ marginLeft: '10px', marginTop: '15px' }}>
                              <div className="col s6">
                                  <label><strong>First Name</strong></label>
                                  <p>{data.firstName}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Last Name</strong></label>
                                  <p>{data.lastName}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Email</strong></label>
                                  <p>{data.email}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Sex</strong></label>
                                  <p>{this.getSexList().find(s => s.value==data.sex).label}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Activate</strong></label>
                                  <p>{data.activate? 'Yes': 'No'}</p>
                              </div>
                              <div className="col s6">
                                  <label><strong>Comments</strong></label>
                                  <p>{data.comments? data.comments : '-'}</p>
                              </div>
                          </div>
               });
    }

    closeMessage = () => {
        this.setState({ showSuccessRegisterMessage: false });
    }

    getSelectFieldList = () => {
      let list = [
        { label:'company1',value:1 },
        { label:'company2',value:2 },
        { label:'company3',value:3 }
      ];
      return list;
    }

    render(){
            return (
                <div>
                    <div className="section-title-container row">
                        <div className="container">
                            <h4>Example 1: Use of Components in Form</h4>
                        </div>
                    </div>
                    <div className="container">
                       <form ref="form" onSubmit={this.props.handleSubmit(this.handleSubmit.bind(this))}>
                        <div className="row">
                            <div className="col s12 m8">
                                <div className="panel-container">
                                        <span className="col s12 m9 title">Enter the personal data of the employee</span>
                                        <TextField userType={currentUserType} maxLength={'10'} name="firstName" size="12 m6 l6" label="First Name" clearButton={false} validate={GeneralValidations.required}/>
                                        <TextField name="lastName" size="12 m6 l6" label="Last Name" clearButton={false} validate={GeneralValidations.required}/>
                                        <TextField name="email" size="12 m6 l6" label="Email" clearButton={false} validate={[GeneralValidations.required, GeneralValidations.email]}/>
                                        <NetsuitePasswordField
                                            userType={currentUserType}
                                            name="password"
                                            label="Enter your password"
                                        />
                                        <NetsuitePasswordField
                                        userType={currentUserType}
                                            name="rePassword"
                                            label="Enter your password"
                                            validate={rePassword}
                                        />
                                        <SelectField autocomplete multi name="company" size="12 m6 l6" label="Company" items={this.getSelectFieldList()} validate={GeneralValidations.required} clearButton/>
                                        <Autocomplete  name="sex" size="12 m6 l6" label="Sex" items={this.getSexList()} validate={GeneralValidations.required} clearButton matchProp={'label'}/>
                                        <DatePickerWithInput clearButton key="3" name="birthDate" label="Birth Date" size="col s12 m6 l6"/>,
                                        <Switch size="col s12 m6 l6" name="activate" title="Activate" titlePosition="right" marginTop="16px" />
                                        <Switch disabled size="col s12 m6 l6" name="activate" title="Deactivate" titlePosition="right" marginTop="16px" />
                                        <div className="col m12" />
                                        <div className="col m6">
                                            <label className="static-input-label">Comments</label>
                                            <div className="form-field-container">
                                                <HtmlTextEditor name="comments" placeholder="Enter your comments..."/>
                                            </div>
                                        </div>
                                        <div className="col m6">
                                            <label className="static-input-label">Photo</label>
                                            <div className="form-field-container">
                                                <UploadFile size="12 m12 l4" 
                                                  validate={GeneralValidations.required} 
                                                  name="photo"
                                                  showDownloadButton
                                                  multiple={false}
                                                  clearButton
                                                  allowedFileTypes=".png, .jpeg, .gif, .jpg, .zip"
                                                  maxSize={2097152}  
                                                  label="Attach your photo (.png, .jpeg, .gif .jpg)"
                                                />
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div className="col s12 m4">
                                <div className="bottom-buttons-container stacked">
                                   <Button onClick={() => this.setState({ showSuccessRegisterMessage:true })} label={'Open Modal'} className="submit" />
                                </div>
                            </div>
                            <div className="col s12 m4">
                                <div className="bottom-buttons-container stacked">
                                   <Button type="submit" label={'Submit'} className="submit" />
                                </div>
                            </div>
                        </div>
                       </form>
                        <Modal
                        userType={currentUserType}
                            title={'Success Register!!!'}
                            bodyClass={'customModalClass'}
                            open={this.state.showSuccessRegisterMessage}
                            labelCancel={'Close'}
                            content={this.state.registerMessage}
                            onRequestClose={() => this.setState({ showSuccessRegisterMessage:false })}
                            actions={[
                                <Button type="button"
                                        label="Close"
                                        width="20"
                                        className="right"
                                        float="right"
                                        marginBottomTop="6"
                                        onClick={(e) => this.closeMessage()}
                                        show
                                        userType={currentUserType}
                                        marginRight="10"/>]}
                            width= "40%"
                            maxHeight= "2%"
                        />
                    </div>
                </div>
            );

    }
}

Form.propTypes = propTypes;

Form = reduxForm({
    form:'form'
})(Form);



export default connect(null, { initialize })(Form);