import React from 'react';
import { getListsData } from '../data/data';
import { FormattedDate } from 'react-intl';
import DatePickerWithInput from '../../src/components/DatePickerWithInput';
import SortByComponent from '../../src/components/SortByComponent';
import AutoComplete from '../../src/components/AutoComplete';
import SelectField from '../../src/components/SelectField';
import Filters from '../../src/components/Filters';
import Table from '../../src/components/Table';
import TextField from '../../src/components/TextField';
import { currentUserType } from '../data/userType';


class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allLists: [],
            filteredLists: []
        };
    }


    componentWillMount() {
        let lists = getListsData();
        this.setState({ allLists: lists, filteredLists: lists });
    }


    getRowsClass = (row) => {
        return [
            (row) => (row.highPriority ? 'tint-row' : '')
        ];
    }

    getTableConfiguration = () => {
        return [
            {
                Header: 'First Name',
                sortable: true,
                accessor:'firstName',
                tdParams:{ 'data-column-title': 'First Name' },
                thParams:{ 'className':'center' },
                Cell: (props) => (<div>
                    {props.value.firstName}
                </div>)
            },
            {
                Header: 'Last Name',
                accessor:'lastName',
                tdParams:{ 'data-column-title': 'Last Name' },
                Cell: (props) => (<div>
                    {props.value.lastName}
                </div>)
            },
            {
                Header: 'Email',
                accessor:'email',
                tdParams:{ 'data-column-title': 'Email' },
                Cell: (props) => <div> {props.value.email}</div>
            },
            {
                Header: 'Sex',
                accessor:'sex',
                tdParams:{ 'data-column-title': 'sex' },
                Cell: (props) => <div> {props.value.sex}</div>
            },
            {
                Header: 'Favorite Colors',
                accessor:'favoriteColors',
                tdParams:{ 'data-column-title': 'favoriteColors' },
                Cell: (props) => <div> {props.value.favoriteColors.map(c => {return <span>{c} - </span>;})} </div>
            },
            {
                Header: 'Creation Date',
                sortable: true,
                accessor:'creationDate',
                tdParams:{ 'data-column-title': 'date' },
                defaultSort: 'desc',
                Cell: (props) => <div> {<FormattedDate value={props.value.creationDate} day="2-digit" month="2-digit" year="numeric"/>}</div>
            }
        ];
    }

    getExcelConfiguration = () => {
    	return(
        {
          titleFile: 'list.xlsx',
          titleSheet: 'Use of components in list',
          textButton: 'Export to excel',
          classButton: 'btn-coloredborder btn-green sm',
          excelColumns: [
              {
                  Header: 'First Name',
                  accessor:'firstName',
              },
              {
                  Header: 'Last Name',
                  accessor:'lastName',
              },
              {
                  Header: 'Email',
                  accessor:'email',
              },
              {
                  Header: 'Sex',
                  accessor:'sex',
              },
              {
                  Header: 'Favorite Color',
                  accessor:'favoriteColors',
              },
              {
                  Header: 'Creation Date',
                  accessor:'creationDate',
              }
          ]
        });
    }

    getFiltersConfig = () => {
        return {
            'firstName': {},
            'lastName': {},
            'dateFrom': {
                'customKey': 'creationDate',
                'type': 'date',
                'operator':'>='
            },
            'dateTo': {
                'customKey': 'creationDate',
                'type': 'date',
                'operator':'<='
            },
            'favoriteColors': {
                type: 'customFunction',
                customFunction: (row, value) => {
                        if (Array.isArray(value)){
                            return value.map(v => row.favoriteColors.includes(v)).reduce((accumulator, currentValue) => accumulator && currentValue);
                        }else{
                            return row.favoriteColors.includes(value);
                        }
                }
                // customKey:'favoriteColors'
            },
            'freeText': {
                'type': 'freeText',
                fuseOptions:{
                    shouldSort: true,
                    tokenize: true,
                    matchAllTokens: true,
                    threshold: 0,
                    location: 0,
                    distance: 0,
                    maxPatternLength: 32,
                    minMatchCharLength: 1,
                    keys: ['firstName', 'lastName', 'sex']
                }
            },
            'sex': {},
        };
    }

    sortHandler = (sortedElements) => {
        this.setState({ filteredLists: sortedElements.filter(s => this.state.filteredLists.find(l => l.id == s.id)), allLists: sortedElements });
    };

    buildSortItems = () => {
        return [
            { label: 'Descendente' ,criteria: 'creationDate', 'order':'desc', iconMode: false },
            { label: 'Ascendente' ,criteria: 'creationDate', 'order':'asc', iconMode: false }
        ];
    };

    afterApplyFilters = (filterdData,filters) => {
        this.setState({ filteredLists: filterdData });
    }

    getColors = () => {
        return [{ label:'Red', value:'Red' },
            { label:'Blue', value:'Blue' },
            { label:'Green', value:'Green' },
            { label:'Black', value:'Black' },
            {  label:'White', value:'White' },
            {  label:'Yellow', value:'Yellow' },
            {  label:'Brown', value:'Brown' }];
    }

    filtersFields = () => {
		return [
            <AutoComplete userType={currentUserType} clearButton key="1" name="firstName" items={this.state.allLists.filter((object, index, self) => index === self.findIndex((o) => (o.firstName === object.firstName))).map(l =>  {return{ label:l.firstName, value:l.firstName };})} size="3" label="First Name"/>,
            <SelectField userType={currentUserType}  clearButton multi key="2" name="lastName" items={this.state.allLists.filter((object, index, self) => index === self.findIndex((o) => (o.lastName === object.lastName))).map(l =>  {return{ label:l.lastName, value:l.lastName };})} size="3" label="Last Name"/>,
            <DatePickerWithInput userType={currentUserType} clearButton key="3" name="dateFrom" defaultDate={new Date(2018,1,1)} label="From" size="2"/>,
            <DatePickerWithInput clearButton key="4" name="dateTo" label="To" size="2" />,
            <SelectField clearButton key="5" name="sex" items={[{ label: 'Male', value:'Male' },{ label: 'Female',value:'Female' }]}  size="2" label="Sex"/>,
            <SelectField clearButton key="6" name="favoriteColors" items={this.getColors()}  size="2" label="Favorite Color" multi/>,
            <TextField userType={currentUserType} key="7" clearButton name="freeText" size="7" label={'Free Text'}/>
        ];
	};

    genericItemAction = () =>{
        alert('Clicked an item');
    }

    prependItemTest = () => {
        return(
            <button className="btn-coloredborder btn-green sm" onClick={() => this.genericItemAction()}>Prepend Button</button>
        );
    }

    render(){
		return (
            <div>
                <div className="section-title-container row">
                    <div className="container">
                        <h4>Example 2: Use of components in list </h4>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                          <Filters
                            userType={currentUserType}
                            viewHideShowButtons
                            disableAutocomplete
                            fields={this.filtersFields()}
                            afterApplyFilter={this.afterApplyFilters.bind(this)}
                            data={this.state.allLists}
                            containerClass="col s12 m10 filter-container"
                            config={this.getFiltersConfig()}
                            titleButtons={<button type="button" className="btn-coloredborder" style={{ marginRight: '5px' }}>Extra button</button>}
                            />
                        <SortByComponent floatingLabelText={'Creation Date'} data={this.state.allLists} items={this.buildSortItems()} onChangeSort={this.sortHandler} containerClass="col s12 m2 filter-container " iconMode={false} />
                    </div>

                    {this.state.filteredLists.length == 0 ?
                        <div className="empty-list-message">
                            <span className="fa fa-search" />
                            <h3>No lists founds</h3>
                        </div>
                        :
                        <Table
                        userType={currentUserType}
                            containerClass="quotes-list"
                            tableContainerClass="list-container small"
                            pagerContianerClass="list-pager-container"
                            columns={this.getTableConfiguration()}
                            rows={this.state.filteredLists}
                            pageSize={10}
                            rowsClass={this.getRowsClass()}
                            excelConfiguration={this.getExcelConfiguration()}
                            headerPrepends={this.prependItemTest()}
                        />
                    }
                </div>
            </div>
		);
	}
}


export default List;