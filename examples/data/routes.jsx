import Form from '../containers/Form';
import IntranetFrom from '../containers/IntranetForm';
import List from '../containers/List';
import Main from '../containers/Main';

const routes = [
    {
        path: '/',
        component: Main,
        breadcrumb: {
            label: 'Main'
        }
    },
    {
        path: '/form',
        component: Form,
        breadcrumb: {
            label: 'Form'
        }
    },
    {
        path: '/intranet',
        component: IntranetFrom,
        breadcrumb: {
            label: 'IntranetFrom'
        }
    },
    {
        path: '/list',
        component: List,
        breadcrumb: {
            label: 'List'
        }
    },            
];

export default routes;