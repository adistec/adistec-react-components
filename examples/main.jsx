import React from 'react';
import ReactDOM from 'react-dom';
import Main from './containers/Main';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers/index';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './assets/main.scss';
import DevTools from './config/devtools';
import IntlProvider from '../src/providers/IntlProvider';
import { addLocaleData } from 'react-intl';
import es from 'react-intl/locale-data/es';
import pt from 'react-intl/locale-data/pt';
import { getTranslations } from './config/translation.js';

injectTapEventPlugin({
    shouldRejectClick: () => document.body.querySelector('.Select-menu-outer')
});

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#1E2CA2',
        primary2Color: '#44B0F1',
        accent1Color: '#1E2CA2'
    },
    datePicker: {
        selectColor: '#1E2CA2',
        color: '#1E2CA2'
    },
    flatButton: {
        primaryTextColor: '#1E2CA2'
    }
});

const translations = getTranslations();

const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk)
)(createStore);

const getLanguage = () => {
    switch(navigator.language.split('-')[0]) {
        case 'es':
            return 'es';
        case 'pt':
            return 'pt';
        default:
            return 'en';
    }
};

const store = createStoreWithMiddleware(reducers,DevTools.instrument());

addLocaleData([...es, ...pt]);

ReactDOM.render(
    <IntlProvider locale={getLanguage()} messages={translations}>
        <MuiThemeProvider muiTheme={muiTheme}>
            <Provider store={store}>
                <div>
                    <DevTools/>
                    <BrowserRouter>
                        <Route path="/" component={Main}/>
                    </BrowserRouter>    
                </div>
            </Provider>
        </MuiThemeProvider>
     </IntlProvider>
, document.getElementById('app'));
