let webpack = require('webpack');
let path = require('path');

let APP_DIR = path.resolve(__dirname, '../');

//plugin
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('assets/styles-css.css');
const extractSASS = new ExtractTextPlugin('assets/styles-sass.css');

module.exports = {
    entry: APP_DIR + '/examples/main.jsx',
    resolve: {
        extensions: ['.js','.jsx']
    },
    output: {
        filename: '[name].[hash].js',
        chunkFilename: '[name].[hash].js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: APP_DIR,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.css$/,
                use: extractCSS.extract({
                    use: 'css-loader',
                    fallback: 'style-loader',
                })
            }, 
            {
                test: /\.scss$/,
                use: extractSASS.extract({
                    use:[
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                  path: './postcss.config.js'
                                }
                            }
                        },
                        { loader: 'sass-loader' }
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    {
                        loader:'file-loader',
                        options:{
                            name: 'assets/img/[name].[hash].[ext]',
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        query: {
                            gifsicle: {
                                interlaced: false
                            },
                            optipng: {
                                optimizationLevel: 7
                            }
                        }
                    }
                ]
            },
           {
              test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
              loader: 'url-loader',
              query: {
                limit: 10000,
                mimetype: 'application/font-woff',
                name: 'fonts/[name].[hash:7].[ext]'
              }
            },
            {
              test: /\.(eot|ttf|otf)(\?.*)?$/,
              loader: 'url-loader',
              query: {
                limit: 10000,
                name: 'fonts/[name].[hash:7].[ext]'
              }
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            // Required
            inject: false,
            template: require('html-webpack-template'),
            // Optional
            appMountId: 'app',
            meta: [
            {
                charset: 'utf-8',
            },
            {
                name:'viewport',
                content:'width=device-width, initial-scale=1.0'
            },
            {
                name:'description',
                content:'Adistec Components'
            },
            {
                name:'author',
                content:'Adistec'
            },
            {
                name:'msapplication-TileColor',
                content:'#FFFFFF'
            },
            {
                name:'msapplication-TileImage',
                content:'/assets/img/favicon/mstile-144x144.png'
            },
            ],
            lang: 'en',
            links: [
                'https://fonts.googleapis.com/css?family=Roboto',
                {
                    href: '/assets/img/favicon/apple-icon-57x57.png',
                    rel: 'icon',
                    sizes: '57x57'
                },
                {
                    href: '/assets/img/favicon/apple-icon-72x72.png',
                    rel: 'icon',
                    sizes: '72x72'
                },
                {
                    href: '/assets/img/favicon/apple-icon-114x114.png',
                    rel: 'apple-touch-icon-precomposed',
                    sizes: '114x114'
                },
                {
                    href: '/assets/img/favicon/favicon-32x32.png',
                    rel: 'icon',
                    sizes: '32x32'
                },
                {
                    href: '/assets/img/favicon/favicon-16x16.png',
                    rel: 'icon',
                    sizes: '32x32'
                },
                {
                    href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
                    rel: 'stylesheet'
                }
              ],
            title:'Adistec Components',
      // And any other config options from html-webpack-plugin:
      // https://github.com/ampedandwired/html-webpack-plugin#configuration
    }),
    new webpack.HotModuleReplacementPlugin(),
    extractCSS,
    extractSASS
    ],
    devServer: {
        historyApiFallback: true,
        port: 3001,
        hot: true
    },
    devtool:'source-map',
    watch: true
};